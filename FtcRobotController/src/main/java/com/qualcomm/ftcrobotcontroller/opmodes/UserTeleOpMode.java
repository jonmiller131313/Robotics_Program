package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

/**
 * This class is for the user controlled phase of the competition round.
 *
 * @author Jonathan Miller
 */
public class UserTeleOpMode extends OpMode{

	private DcMotor leftDriveMotor, rightDriveMotor, armExtenderMotor, leftArmRotatorMotor, rightArmRotatorMotor;

	private final boolean DOWNSHIFT_ENABLED = true;
	private final double DOWNSHIFT_CONSTANT = .3;
	private final double ROTATE_CONSTANT = .1;
	private final double EXTEND_CONSTANT = .3;
	private final double LOCK_CONSTANT = .2;

	/**
	 * This method is called when the OpMode is loaded.<p>
	 * Put code in here that you want the robot to accomplish first.
	 */
	@Override
	public void init() {
		// Loading the motors from the hardwareMap
		leftDriveMotor = hardwareMap.dcMotor.get("Drive_L");
		rightDriveMotor = hardwareMap.dcMotor.get("Drive_R");
		armExtenderMotor = hardwareMap.dcMotor.get("Extender");
		leftArmRotatorMotor = hardwareMap.dcMotor.get("Rotator_L");
		rightArmRotatorMotor = hardwareMap.dcMotor.get("Rotator_R");
	}

	/**
	 * The main program's loop. Every few milliseconds (10-20) the program executes code in this method.<p>
	 * Make sure not to have the program wait on anything (Thread.sleep()) or else bad stuff happens.
	 */
	@Override
	public void loop() {
		/*
		 * Gamepad 1 is the controller used to control the robot.
		 *
		 * The left stick and right sticks are used to control the motors like a tank.
		 * Up and down on the gamepad tell the motors to work at the same time (forwards and backwards).
		 * Left and right on the gamepad tell the motors to work opposite of one another turning (to turn left/right respectively).
		 * The right bumper button is a "downshift" button that tell the motors to use less power than what's actually given from the gamepad,
		 * resulting in the robot moving slower at "full speed ahead" while the bumper is held down.
		 *
		 * Gamepad 2 is the controller used to control the robot's push(up/down) arm, and the scoop arm.
		 * Y and A raise and lower the push arms.
		 * LB and RB raise and lower the scoop arm
		 */

		// Setting the throttle of both motors to 0 (also declaring the variables).
		float throttleLeft = 0;
		float throttleRight = 0;

		if (gamepad1.dpad_up) {  // If up on the dpad is pressed then both motors are full power forward.
			throttleLeft = 1;
			throttleRight = 1;
		} else if(gamepad1.dpad_down) { // If down on the dpad is pressed then both motors are full power backwards.
			throttleLeft = -1;
			throttleRight = -1;
		} else if(gamepad1.dpad_left) { // If left on the dpad is pressed then the left motor is reversed and right is forward.
			throttleLeft = -1;
			throttleRight = 1;
		} else if(gamepad1.dpad_right) { // If right on the dpad is pressed then the left motor is forward and right is backward.
			throttleLeft = 1;
			throttleRight = -1;
		} else {                // Else, look at the y-axis of the left and right joysticks for each motor (respectively).
			throttleLeft = -gamepad1.left_stick_y;  // -1 is full up, and 1 is full down.
			throttleRight = -gamepad1.right_stick_y;

			// Scale the joystick values to make it easier to control the robot more precisely at slower speeds.
			throttleLeft = (float) scaleDriveInput(throttleLeft);
			throttleRight = (float) scaleDriveInput(throttleRight);

		}

		if(gamepad1.right_bumper && DOWNSHIFT_ENABLED) { // If the right bumper button is pressed, and downshift is enabled
			// Reduce the power given to the drive motors by 50%.
			throttleLeft *= DOWNSHIFT_CONSTANT;
			throttleRight *= DOWNSHIFT_CONSTANT;
		}

		// Now for the 2nd game controller.

		// The arm tip rotator
		double rotatorPower = -gamepad2.left_stick_y;
		rotatorPower = scaleRotorInput(rotatorPower);

//		if(gamepad2.x)
//			rotatorPower = -LOCK_CONSTANT;

		// Specifying what the rotators motors are doing
		String rotatorStatus;
		if(gamepad2.x)
			rotatorStatus = "Locked";
		else if(rotatorPower > 0)
			rotatorStatus = "Rotating down";
		else if(rotatorPower < 0)
			rotatorStatus = "Rotating up";
		else
			rotatorStatus = "Holding";

		// The arm extender
		double extenderPower;
		if(gamepad2.left_bumper)
			extenderPower = EXTEND_CONSTANT;
		else if(gamepad2.right_bumper)
			extenderPower = -EXTEND_CONSTANT;
		else
			extenderPower = 0;

		// Displaying and assigning motor information
		telemetry.addData("Downshifting", (DOWNSHIFT_ENABLED ? gamepad1.right_bumper : "DISABLED"));
		telemetry.addData("Drive Motors", throttleLeft + "=L R=" + throttleRight);
		telemetry.addData("Rotator", rotatorStatus  + "(" +  rotatorPower + ")");
		telemetry.addData("Extender", (extenderPower == 0 ? "Holding" : (extenderPower > 0 ? "Retracting" : "Extending")));

		leftDriveMotor.setPower(-throttleLeft);
		rightDriveMotor.setPower(throttleRight);
		leftArmRotatorMotor.setPower(rotatorPower);
		rightArmRotatorMotor.setPower(-rotatorPower);
		armExtenderMotor.setPower(extenderPower);
	}

	/*
	 * This method scales the joystick input so for low joystick values, the
	 * scaled value is less than linear.  This is to make it easier to rotate
	 * the robot's arm more precisely and smoothly.
	 */
	double scaleRotorInput(double dVal)  {
		double[] scaleArray = {0.0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07};

		// get the corresponding index for the scaleDriveInput array.
		int index = (int) (dVal * 7.0);

		// index should be positive.
		if (index < 0) {
			index = -index;
		}

		// index cannot exceed size of array minus 1.
		if (index > 7) {
			index = 7;
		}

		// get value from the array.
		double dScale = 0.0;
		if (dVal < 0) {
			dScale = -scaleArray[index];
		} else {
			dScale = scaleArray[index];
		}

		// return scaled value.
		return dScale;
	}

	/*
	 * This method scales the joystick input so for low joystick values, the
	 * scaled value is less than linear.  This is to make it easier to drive
	 * the robot more precisely at slower speeds.
	 */
	double scaleDriveInput(double dVal)  {
		double[] scaleArray = {0.0, 0.05, 0.09, 0.10, 0.12, 0.15, 0.18, 0.24,
				0.30, 0.36, 0.43, 0.50, 0.60, 0.72, 0.85, 1.00, 1.00};

		// get the corresponding index for the scaleDriveInput array.
		int index = (int) (dVal * 16.0);

		// index should be positive.
		if (index < 0) {
			index = -index;
		}

		// index cannot exceed size of array minus 1.
		if (index > 16) {
			index = 16;
		}

		// get value from the array.
		double dScale = 0.0;
		if (dVal < 0) {
			dScale = -scaleArray[index];
		} else {
			dScale = scaleArray[index];
		}

		// return scaled value.
		return dScale;
	}
}
