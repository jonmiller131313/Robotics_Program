package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

/**
 * This class is for the 30 second auto phase of the beginning of the competition round.
 *
 * @author Jonathan Miller
 */
public class UserAutoOpMode extends LinearOpMode {

	private DcMotor leftDriveMotor, rightDriveMotor, armExtenderMotor, leftArmRotatorMotor, rightArmRotatorMotor;

	private final double[] TURNING_CONSTANTS = new double[] {.2, .5, .7, 1.0};

	private final double DRIVE_CONSTANT = .5;

	/*
	 * Objectives:
	 *  Button push
	 *  Divers in bin
	 *  Park on ram
	 *  Push blocks?
	 */

	@Override
	public void runOpMode() {
		gatherHardwareInfo();
		forward(2);
		turnRight(2);
		forward(4);
		turnLeft(3);
	}

	private void forward(double seconds) {
		status("Driving forward for (" + seconds + " seconds )...");
		go();
		hold(seconds);
		status("Complete");
		halt();
	}

	private void turnRight(int index) {
		String turningMagnitude = "";
		switch(index) {
			case 0:
				turningMagnitude = "slightly";
				break;
			case 1:
				turningMagnitude = "a little";
				break;
			case 3:
				turningMagnitude = "moderately";
				break;
			case 4:
				turningMagnitude = "a lot";
				break;
		}
		status("Turning " + turningMagnitude);
		right();
		hold(TURNING_CONSTANTS[index]);
		status("Complete");
		halt();
	}

	private void turnLeft(int index) {
		String turningMagnitude = "";
		switch(index) {
			case 0:
				turningMagnitude = "slightly";
				break;
			case 1:
				turningMagnitude = "a little";
				break;
			case 3:
				turningMagnitude = "moderately";
				break;
			case 4:
				turningMagnitude = "a lot";
				break;
		}
		status("Turning " + turningMagnitude);
		left();
		hold(TURNING_CONSTANTS[index]);
		status("Complete");
		halt();
	}

	private void gatherHardwareInfo() {
		status("Gathering hardware info...");
		hold(.5);
		leftDriveMotor = hardwareMap.dcMotor.get("Drive_L");
		rightDriveMotor = hardwareMap.dcMotor.get("Drive_R");
		armExtenderMotor = hardwareMap.dcMotor.get("Extender");
		leftArmRotatorMotor = hardwareMap.dcMotor.get("Rotator_L");
		rightArmRotatorMotor = hardwareMap.dcMotor.get("Rotator_R");
		status("Done");
		hold(.5);
	}

	private void status(String message) {
		telemetry.addData("Status", message);
	}

	private void motorStatus(String message) {
		telemetry.addData("Motor status", message);
	}

	private void hold(double seconds) {
		try{
			Thread.sleep((long) (seconds * 1000.0));
		} catch(InterruptedException e) {
			status("Thread.sleep() interrupted!!!");
		}
	}

	private void go() {
		motorStatus("Forward");
		leftDriveMotor.setPower(-DRIVE_CONSTANT);
		rightDriveMotor.setPower(DRIVE_CONSTANT);
	}

	private void halt() {
		motorStatus("Halted");
		leftDriveMotor.setPower(0);
		rightDriveMotor.setPower(0);
	}

	private void right() {
		motorStatus("Turning right");
		leftDriveMotor.setPower(-DRIVE_CONSTANT);
		rightDriveMotor.setPower(-DRIVE_CONSTANT);
	}

	private void left() {
		motorStatus("Turning left");
		leftDriveMotor.setPower(DRIVE_CONSTANT);
		rightDriveMotor.setPower(DRIVE_CONSTANT);
	}
}
