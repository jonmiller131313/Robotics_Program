package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

/**
 * This class is a test Op Mode for electronic configurations, motor testing, etc.
 *
 * @author Jonathan Miller
 */
public class UserTestMode extends OpMode {

	private DcMotor leftMotor;
	private DcMotor rightMotor;


	/**
	 * This method is called when the OpMode is loaded.<p>
	 * Put code in here that you want the robot to accomplish first.
	 */
	@Override
	public void init() {
		// Loading the motors from the hardwareMap
		leftMotor = hardwareMap.dcMotor.get("Motor_L");
		rightMotor = hardwareMap.dcMotor.get("Motor_R");
	}

	/**
	 * This method is called when the user first starts the round<p>
	 * Put code here that you want the robot to accomplish as the round begins.
	 */
	@Override
	public void start() {

	}

	/**
	 * The main program's loop. Every few milliseconds (10-20) the program executes code in this method.<p>
	 * Make sure not to have the program wait on anything (Thread.sleep()) or else bad stuff happens.
	 */
	@Override
	public void loop() {
		/*
		 * Gamepad 1 is the controller used to control the robot.
		 *
		 * The left stick and right sticks are used to control the motors, respectively.
		 * Up and down on the gamepad tell the motors to work at the same time (forwards and backwards).
		 */

		// Setting the throttle of both motors to 0 (also declaring the variables).
		float throttleLeft = 0;
		float throttleRight = 0;

		if(gamepad1.dpad_up) {  // If up on the dpad is pressed then both motors are full power forward.
			throttleLeft = 1;
			throttleRight = 1;
		} else if(gamepad1.dpad_down) { // If down on the dpad is pressed then both motors are full power backwards.
			throttleLeft = -1;
			throttleRight = -1;
		} else {                // Else, look at the y-axis of the left and right joysticks for each motor (respectively).
			throttleLeft = -gamepad1.left_stick_y;  // -1 is full up, and 1 is full down.
			throttleRight = -gamepad1.right_stick_y;

			// Scale the joystick values to make it easier to control the robot more precisely at slower speeds.
			throttleLeft = (float) scaleInput(throttleLeft);
			throttleRight = (float) scaleInput(throttleRight);
		}

		telemetry.addData("Left", throttleLeft);
		telemetry.addData("Right", throttleRight);
		leftMotor.setPower(throttleLeft);
		rightMotor.setPower(throttleRight);
	}

	/**
	 * This method is called when the round ends or an emergency stop() is called.<p>
	 * Put code here to clean up when everything is finished.
	 */
	@Override
	public void stop() {

	}

	/*
	 * This method scales the joystick input so for low joystick values, the
	 * scaled value is less than linear.  This is to make it easier to drive
	 * the robot more precisely at slower speeds.
	 */
	double scaleInput(double dVal)  {
		double[] scaleArray = { 0.0, 0.05, 0.09, 0.10, 0.12, 0.15, 0.18, 0.24,
				0.30, 0.36, 0.43, 0.50, 0.60, 0.72, 0.85, 1.00, 1.00 };

		// get the corresponding index for the scaleDriveInput array.
		int index = (int) (dVal * 16.0);

		// index should be positive.
		if (index < 0) {
			index = -index;
		}

		// index cannot exceed size of array minus 1.
		if (index > 16) {
			index = 16;
		}

		// get value from the array.
		double dScale = 0.0;
		if (dVal < 0) {
			dScale = -scaleArray[index];
		} else {
			dScale = scaleArray[index];
		}

		// return scaled value.
		return dScale;
	}

	/**
	 * Constructor
	 */
	public UserTestMode() {
		// Shouldn't be called except from the app itself when the OpMode is loaded
	}
}
