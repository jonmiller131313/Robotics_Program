A program to control a robot. (Chap 9.3 in FTC [training manual](http://www.firstisrael.org.il/userfiles/file/FTC/2016/Resources/FTCTraining%20Manual%20v0_93.pdf))

**Links:**
- [Original Repo](https://github.com/ftctechnh/ftc_app)
- [FTC Website](http://www.firstinspires.org/robotics/ftc)
- [Video](https://youtu.be/b49Q3kqrSv0?t=163)
